// based on @ccjmk macro for sleep, retooled for Color Spray. Gets targets and then blinds them.
(async ()=>{
async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}
let csprayHp = await args[0].damageTotal;

if(!csprayHp) {
  ui.notifications.error("No arguments passed to Color Spray macro");
  return;
}
console.log(`starting Color Spray macro with csprayHp[${csprayHp}]`);

// Get Targets
let targets = await args[0].targets.sort((a,b) => canvas.tokens.get(a._id).actor.data.data.attributes.hp.value < canvas.tokens.get(b._id).actor.data.data.attributes.hp.value ? -1 : 1);
let remainingSleepHp = csprayHp;
const condition = "Blinded";
const immnue_condition = "Unconscious";
let blind_target = [];

for(let target of targets){
   let find_target = await canvas.tokens.placeables.find(t=>t.name===target.name);
  let targetHpValue = find_target.actor.data.data.attributes.hp.value;
  if ((game.cub.hasCondition(condition, find_target)) && (game.cub.hasCondition(immnue_condition, find_target))){
    console.log(`${target.name} with hp ${targetHpValue} resists Color Spray, skipping remaining targets.`);
    blind_target.push(`<div class="midi-qol-flex-container"><div>misses</div><div class="midi-qol-target-npc midi-qol-target-name" id="${find_target.id}"> ${find_target.name}</div><div><img src="${find_target.data.img}" width="30" height="30" style="border:0px"></div></div>`);
    continue;
  }
  if(remainingSleepHp > targetHpValue){
    remainingSleepHp -= targetHpValue;
    console.log(`${target.name} with hp ${targetHpValue} falls Color Spray, remaining hp from dice ${remainingSleepHp}.`);
    blind_target.push(`<div class="midi-qol-flex-container"><div>hits</div><div class="midi-qol-target-npc midi-qol-target-name" id="${find_target.id}"> ${find_target.name}</div><div><img src="${find_target.data.img}" width="30" height="30" style="border:0px"></div></div>`);
    // Apply Blinded condition to target
    game.cub.addCondition(condition, find_target);
  } else {
    console.log(`${target.name} with hp ${targetHpValue} resists Color Spray, skipping remaining targets.`);
    blind_target.push(`<div class="midi-qol-flex-container"><div>misses</div><div class="midi-qol-target-npc midi-qol-target-name" id="${find_target.id}"> ${find_target.name}</div><div><img src="${find_target.data.img}" width="30" height="30" style="border:0px"></div></div>`);
    break;
  }
}
await wait(500);
let blind_list = blind_target.join('');
let blind_results = `<div><div class="midi-qol-nobox">${blind_list}</div></div>`;
const chatMessage = await game.messages.get(args[0].itemCardId);
let content = await duplicate(chatMessage.data.content);
const searchString =  '<div class="midi-qol-hits-display"><div class="end-midi-qol-hits-display"></div></div>';
const replaceString = `<div class="midi-qol-hits-display"><div class="end-midi-qol-hits-display">${blind_results}</div></div>`;
content = await content.replace(searchString, replaceString);
await chatMessage.update({content: content});
})();
