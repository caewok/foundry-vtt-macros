//###############################################################
// READ First!
// Themed after Kandashi's create item macro
// This macro creates a spell called "Melf's Minute Meteor". The hotbar macro chedck to see if the spell exists.
// Requires Item Macro.
//###############################################################

//##############################################################
// Hotbar Macro
//#############################################################
let melf_full = "Melf's Minute Meteors";
let melf_single = "Melf's Minute Meteor";
let target = canvas.tokens.controlled[0].actor || game.user.character;
if(target.items.find(i => i.name === melf_single)){
	game.dnd5e.rollItemMacro(melf_single);
} else {
	game.dnd5e.rollItemMacro(melf_full);
}

//#############################################################
// DAE macro execute @item.level
//############################################################
const lastArg = args[args.length - 1];
let tactor;
if (lastArg.tokenId) tactor = canvas.tokens.get(lastArg.tokenId).actor;
else tactor = game.actors.get(lastArg.actorId);
const target = canvas.tokens.get(lastArg.tokenId);
const saveData = tactor.data.data.attributes.spelldc;
const itemD = lastArg.efData.flags.dae.itemData;
const level = args[1];

if (args[0] === "on") {
  let meteorCount = Number(level*2);
  await target.actor.createOwnedItem({
  "name": "Melf's Minute Meteor",
  "type": "spell",
  "data": {
    "description": {
      "value": "<p>Once a meteor reaches its destination or impacts against a solid surface, the meteor explodes. Each creature within 5 feet of the point where the meteor explodes must make a Dexterity saving throw. A creature takes 2d6 fire damage on a failed save, or half as much damage on a successful one.</p>",
      "chat": "",
      "unidentified": ""
    },
    "source": "",
    "activation": {
      "type": "bonus",
      "cost": 1,
      "condition": ""
    },
    "duration": {
      "value": null,
      "units": ""
    },
    "target": {
      "value": 15,
      "width": null,
      "units": "ft",
      "type": "square"
    },
    "range": {
      "value": 120,
      "long": null,
      "units": "ft"
    },
    "uses": {
      "value": meteorCount,
      "max": meteorCount,
      "per": "charges"
    },
    "consume": {
      "type": "",
      "target": "",
      "amount": null
    },
    "ability": "",
    "actionType": "save",
    "attackBonus": 0,
    "chatFlavor": "",
    "critical": null,
    "damage": {
      "parts": [
        [
          "2d6",
          "fire"
        ]
      ],
      "versatile": ""
    },
    "formula": "",
    "save": {
      "ability": "dex",
      "dc": saveData,
      "scaling": "spell"
    },
    "level": 1,
    "school": "evo",
    "components": {
      "value": "",
      "vocal": true,
      "somatic": true,
      "material": true,
      "ritual": false,
      "concentration": false
    },
    "materials": {
      "value": "",
      "consumed": false,
      "cost": 0,
      "supply": 0
    },
    "preparation": {
      "mode": "innate",
      "prepared": true
    },
    "scaling": {
      "mode": "none",
      "formula": ""
    },
    "attributes": {
      "spelldc": 10
    }
  },
  "sort": 15200000,
  "flags": {
    "dynamiceffects": {
      "equipActive": false,
      "alwaysActive": false,
      "effects": []
    },
    "mess": {
      "templateTexture": ""
    },
    "betterCurses": {
      "isCurse": true,
      "curseName": ",",
      "formula": ",",
      "mwak": true,
      "rwak": true,
      "msak": true,
      "rsak": true
    },
    "midi-qol": {
      "onUseMacroName": "ItemMacro"
    },
    "itemacro": {
      "macro": {
        "_data": {
          "name": "Melf's Minute Meteor",
          "type": "script",
          "scope": "global",
          "command": "(async()=>{\nlet actorD = canvas.tokens.get(args[0].tokenId).actor;\nlet itemD = args[0].item;\nif(itemD.data.uses.value === 0){\n    let conId = actorD.effects.find(i=> i.data.label === \"Concentrating\").id;\n    await actorD.deleteEmbeddedEntity(\"ActiveEffect\", conId);\n}\n})();",
          "author": game.user.id
        },
        "data": {
          "name": "Melf's Minute Meteor",
          "type": "script",
          "scope": "global",
          "command": "(async()=>{\nlet actorD = canvas.tokens.get(args[0].tokenId).actor;\nlet itemD = args[0].item;\nif(itemD.data.uses.value === 0){\n    let conId = actorD.effects.find(i=> i.data.label === \"Concentrating\").id;\n    await actorD.deleteEmbeddedEntity(\"ActiveEffect\", conId);\n}\n})();",
          "author": game.user.id
        },
        "options": {},
        "apps": {},
        "compendium": null
      }
    },
    "dae": {
      "activeEquipped": false,
      "alwaysActive": false
    }
  },
  "img": itemD.img
}
  );
  let item = target.actor.data.items.find(i => i.name === "Melf's Minute Meteor");
  let copy_item = duplicate(item);
  copy_item.data.consume.target = copy_item._id;
  target.actor.updateEmbeddedEntity("OwnedItem", copy_item);
}

if (args[0] === "off") {
  let item = target.actor.data.items.find(i => i.name === "Melf's Minute Meteor");
  target.actor.deleteOwnedItem(item._id);
}
