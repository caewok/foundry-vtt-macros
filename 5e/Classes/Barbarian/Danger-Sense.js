//############################
// Read First!!!!!!!!!!!!!!!!
// MidiQOL "on use" macro
//############################
(async()=>{
const actorD = game.actors.get(args[0].actor._id);
const effectTypes = ["Deafened", "Blinded", "Incapacitated"];
const effects = actorD.effects.filter(i=> effectTypes.includes(i.data.label));
let roll;
effects.length > 0 ? roll = await actorD.rollAbilitySave("dex", {advantage:false, fastForward:true, chatMessage:false}) : roll = await actorD.rollAbilitySave("dex", {advantage:true, fastForward:true, chatMessage:false});
let rollType = effects.length > 0 ? "" : " (Advantage)";
game.dice3d?.showForRoll(roll);
let dice_roll = await roll.dice[0].results;
let rolled = "";
let get_dice = "";           
for (let dice of dice_roll){
  if (dice.discarded){
     get_dice += `<li class="roll die d20 discarded">${dice.result}</li>`;
 }
 else if((dice.active) && (dice.result === 20)){
     get_dice += `<li class="roll die d20 success">${dice.result}</li>`;
     rolled = "critical";
 }
 else if((dice.active) && (dice.result === 1)){
     get_dice += `<li class="roll die d20 fumble">${dice.result}</li>`;
     rolled = "fumble";
 }
 else {
 get_dice += `<li class="roll die d20">${dice.result}</li>`;
 }
}
let roll_results = `<div class="dice-roll"><p>Dexterity Saving Throw${rollType}</p><div class="dice-result"><div class="dice-formula">${roll.formula}</div><div class="dice-tooltip"><div class="dice"><header class="part-header flexrow"><span class="part-formula">${roll.formula}</span><span class="part-total">${roll.total}</span></header><ol class="dice-rolls">${get_dice}</ol></div></div><h4 class="dice-total ${rolled}">${roll.total}</h4></div></div>`;
const chatMessage = game.messages.get(args[0].itemCardId);
let content = duplicate(chatMessage.data.content);    
const searchString =  /<div class="midi-qol-saves-display">[\s\S]*<div class="end-midi-qol-saves-display">/g;
const replaceString = `<div class="midi-qol-saves-display"><div class="end-midi-qol-saves-display">${roll_results}`;
content = content.replace(searchString, replaceString);
chatMessage.update({content: content});
})();
