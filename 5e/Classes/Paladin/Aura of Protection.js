// Midi-qol / DAE macro.
// Requires Active Effect and Token Magic FX Callback Macro
(async()=>{

async function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}
const ActiveEffect = game.macros.getName("ActiveEffect");
const PaladinAura = game.macros.getName("PaladinAura");
const paladin = await game.actors.get(args[1]._id).getActiveTokens()[0];
const level = paladin.actor.getRollData().classes.paladin.levels;
const charisma_mod = paladin.actor.data.data.abilities.cha.mod != 0 ? paladin.actor.data.data.abilities.cha.mod : 1;
const distance = level >= 18 ? 34.5 : level >= 7 ? 14.5 : 0;
const itemD = args[2];
const image = itemD.img;
const effect_name = itemD.name;

let get_center = paladin.center;

function aoe_aura(paladin, get_center){
let get_texture = 'modules/jaamod/AnimatedArt/Runes/teleportationCircle4.webm';
let findTile = canvas.tiles.placeables.find(i=> i.data.img === get_texture);
let combined_distance = canvas.grid.grid.options.dimensions.distance*canvas.grid.grid.options.dimensions.size;
let combined_half_x = get_center.x - combined_distance/2;
let combined_half_y = get_center.y - combined_distance/2;

if(!findTile){
    let tileData = [{
        img: get_texture,
        width: combined_distance,
        height: combined_distance,
        scale: paladin.data.scale,
        x: combined_half_x,
        y: combined_half_y,
        z: 100,
        rotation: 0,
        hidden: false,
        locked: false
    }];
    Tile.create(tileData);
  }
  else {
    findTile.update({"x": combined_half_x, "y": combined_half_y});
  }
}


async function get_targets(paladin, target, distance, effect_name, image){
  let get_target = await canvas.tokens.placeables.filter(target => (canvas.grid.measureDistance(paladin.center, target.center) <= distance && paladin.id != target.id && paladin.data.disposition === target.data.disposition && !canvas.walls.checkCollision(new Ray(paladin.center, target.center))));
  console.log("Add Targets");
  console.log(get_target);
  for(let target of get_target){
    await apply_aura(target, effect_name, image);
  }
}

async function remove_targets(paladin, target, distance, effect_name){
  let get_target = await canvas.tokens.placeables.filter(target => (canvas.grid.measureDistance(target.center, paladin.center) > distance && paladin.id != target.id && paladin.data.disposition === target.data.disposition));
  console.log("Remove Targets");
  console.log(get_target);
  for(let target  of get_target){
    await remove_aura(target, effect_name);
  }
}

async function apply_aura(target, effect_name, image){
  const effectData = {
    label : effect_name,
    icon : image,
    changes: [{
      "key": "data.bonuses.abilities.save",
      "mode": 2,
      "value": `+${charisma_mod}`,
      "priority": 0
    }]
  };
  if(await target.actor.effects.find(ef=> ef.data.label === effect_name)) return console.log(`${target.name} already has ${effect_name}`);
   await ActiveEffect.execute(target.id, effectData, "add");
   await PaladinAura.execute(target.id, "add");
}

async function remove_aura(target, effect_name){
  if(await target.actor.effects.find(ef=> ef.data.label === effect_name)){
   await ActiveEffect.execute(target.id, effect_name, "remove");
   await PaladinAura.execute(target.id, "remove");
  }
}

async function auraMovement(scene, token, update, flags, id) {
  let target = await canvas.tokens.get(token._id);
  let movement = await getProperty(update, "x") || await getProperty(update, "y");
  if (movement !== undefined) {
    if (target.id === paladin.id) get_center = paladin.center;
    aoe_aura(paladin, get_center);
    remove_targets(paladin, target, distance, effect_name);
    get_targets(paladin, target, distance, effect_name, image);
 }
}

if(args[0] === "on"){
const hookId = Hooks.on("updateToken", auraMovement);
DAE.setFlag(paladin.actor, "AuraProtection", hookId);
aoe_aura(paladin, get_center);
}

if(args[0] === "off"){
const hookId = DAE.getFlag(paladin.actor, "AuraProtection");
Hooks.off("updateToken", hookId);
DAE.unsetFlag(paladin.actor, "AuraProtection");
let get_texture = 'modules/jaamod/AnimatedArt/Runes/teleportationCircle4.webm';
let findTile = canvas.tiles.placeables.find(i=> i.data.img === get_texture);
findTile.delete();
}
})();
