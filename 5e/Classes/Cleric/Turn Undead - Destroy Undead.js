// Thanks to Kandashi and Tposney for the help. Uses GM macro = > https://github.com/kandashi/Macros/blob/master/Callback%20Macros/ActorUpdate.js and my Cub update macro.
// Item Macro @target @classes.cleric.levels @item @abilities.wis.dc
// Will check if monster is undead and compare it's CR, then drops them to 0 hp.
// Remember to remove everything from the bottom part of setup, so this rolls the saving throw instead of the item.
if (args[0] === "on") {
let target = canvas.tokens.get(args[1]);
let level = args[2];
let itemD = args[3];
let myDC = args[4];
let mon_cr = target.actor.data.data.details.cr;
let ActorUpdate = game.macros.getName("ActorUpdate");
let undead = ["undead"].some(type => (target.actor.data.data.details.type || "").toLowerCase().includes(type));
function cr_lookup(level) {
if ((level >= 5) && (level < 8)) {return 0.5;}
if ((level >= 8) && (level < 11)) {return 1;}
if ((level >= 11) && (level < 14)) {return 2;}
if ((level >= 14) && (level < 17)) {return 3;}
if ((level >= 17) && (level <= 20)) {return 4;}
}
let level_cr = cr_lookup(level);

if (undead) {
let save_dice = `1d20 + ${target.actor.data.data.abilities.wis.save} + ${target.actor.data.data.attributes.prof}`;
let save_roll = new Roll(`1d20 + ${target.actor.data.data.abilities.wis.save} + ${target.actor.data.data.attributes.prof}`).roll();
let results_html = `<div class="dnd5e chat-card item-card"><header class="card-header flexrow"><img src="${itemD.img}" title="${itemD.name}" width="36" height="36"><h3 class="item-name">${itemD.name}</h3></header><div class="card-content">${itemD.data.description.chat}</div>
<div class="card-buttons"><div class="flexrow 1"><div style="text-align:center;text-transform:capitalize;">Wisdom Saving Throw (DC ${myDC})<div class="dice-roll"><div class="dice-result"><div class="dice-formula">${save_dice}</div><h4 class="dice-total">${save_roll.total}</h4></div></div></div></div></div><footer class="card-footer"><span>${itemD.data.level} Level</span><span>V</span><span>${itemD.data.activation.cost} ${itemD.data.activation.type} Action</span><span>${itemD.data.target.value} ${itemD.data.target.type}</span><span>${itemD.data.range.value} ${itemD.data.range.units}</span><span>${itemD.data.duration.value} ${itemD.data.duration.units}</span></footer></div>`;
ChatMessage.create({
					        user: game.user._id,
                            speaker: ChatMessage.getSpeaker({token: target}),
                            content: results_html
});
if(save_roll.total < myDC) {
	ui.notifications.warn(`${target.name} successfully saved a ${save_roll.total} (DC ${itemD.data.save.dc}).`);
    if (mon_cr <= level_cr) {  
    ActorUpdate.execute(target.id,{"data.attributes.hp.value": 0});	
    }
else {
  game.macros.getName("Cub_Condition").execute(target.data._id, "Frightened", "add");
     }
   }
 }
}